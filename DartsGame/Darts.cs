﻿using System;

namespace DartsGame
{
    public static class Darts
    {
        /// <summary>
        /// Calculates the earned points in a single toss of a Darts game.
        /// </summary>
        /// <param name="x">x-coordinate of dart.</param>
        /// <param name="y">y-coordinate of dart.</param>
        /// <returns>The earned points.</returns>
        public static int GetScore(double x, double y)
        {
            int result = 0;
            double dist = Math.Sqrt((x * x) + (y * y));

            if (dist <= 1)
            {
                result = 10;
            }
            else if (dist <= 5)
            {
                result = 5;
            }
            else if (dist <= 10)
            {
                result = 1;
            }
            else
            {
                result = 0;
            }

            return result;
        }
    }
}
